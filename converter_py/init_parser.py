import argparse


def parse_init(filename, verbose=False):
    """
    in >> num cell val
    in >> num wall val
    in >> num vertex val

    create a vector (list) of cells
    create list of walls
    create list of vertices

    read connective topology
    loop over walls:
        connect walls to vertices
            connect vertices to wall
        connect cells to walls
            connect walls to cells
        connect cells to vertices
            connect vertices to wall
    
    read and set vertex positions
    """
    f = open(filename, 'r')

    # num_cell_val, num_wall_val, numVertexVal
    l = f.readline().strip().split(" ")
    num_cell_val, num_wall_val, num_vertex_val = int(l[0]), int(l[1]), int(l[2])
    if verbose:
        print('num cells:    ', num_cell_val)
        print('num walls:    ', num_wall_val)
        print('num vertices: ', num_vertex_val)

    if verbose:
        print('=' * 20)
        print('Walls (wall id, cell ids, vertex ids)')
    walls = []
    for i in range(num_wall_val):
        l = f.readline().strip().split(" ")
        if verbose:
            if i < 5 or i > num_wall_val - 5:
                print(l[0], " ", l[1:])
        walls.append(l)

    l = f.readline().strip().split(" ")
    l = f.readline().strip().split(" ")
    if verbose:
        print("=" * 20)
        print('Vertices (vertex id, position)')
    vertices = []
    for j, i in enumerate(range(num_vertex_val)):
        l = f.readline().strip().split(" ")
        if verbose:
            if i < 5 or i > num_vertex_val - 5:
                print(j, " ", l)
        vertices.append([float(i) for i in l])
    l = f.readline().strip().split(" ")
    l = f.readline().strip().split(" ")

    if verbose:
        print("=" * 20)
        print('Walls: (wall id, wall_vars)')
    walls_info = []
    for j, i in enumerate(range(num_wall_val)):
        l = f.readline().strip().split(" ")
        if verbose:
            if i < 5 or i > num_wall_val - 5:
                print(j, l)
        walls_info.append(l)
    l = f.readline().strip().split(" ")
    l = f.readline().strip().split(" ")
    if verbose:
        print("=" * 20)
        print("cell id, number of cell vars")
    cells= []
    for j, i in enumerate(range(num_cell_val)):
        l = f.readline().strip().split(" ")
        if verbose:
            if i < 5 or i > num_cell_val - 5:
                print(j, len(l))
        cells.append(l)
    if verbose:
        print("=" * 20)
    f.close()

    counts = {'cells': num_cell_val, 'walls': num_wall_val, 'vertices': num_vertex_val}
    data = {'walls': walls, 'vertices': vertices, 'walls_info': walls_info, 'cells': cells}

    return counts, data


def apply_connective_topology(counts, data_):
    print('applying connective topology')

    cell_topo = [[[]]*2] * counts['cells']
    wall_topo = [[[]]*2] * counts['walls']
    vertex_topo = [[[]]*2] * counts['vertices']

    for wall in data_['walls']:
        wall_id = wall[0]
        cell_ids = wall[1:3]
        vertex_ids = wall[3:]
        for cell_id in cell_ids:
            cell_topo[int(cell_id)][0].append(wall_id)
            cell_topo[int(cell_id)][1].append(vertex_ids)
        wall_topo[int(wall_id)][0].append(cell_ids)
        wall_topo[int(wall_id)][1].append(vertex_ids)
        for vertex_id in vertex_ids:
            vertex_topo[int(vertex_id)].append(cell_ids)
            vertex_topo[int(vertex_id)].append(wall_id)

    topology_ = {
        'cells': cell_topo,
        'walls': wall_topo,
        'vertices': vertex_topo
    }

    return topology_


def write_init(filename, counts, data):
    # TODO WRITE THIS!
    f = open(filename, 'w')
    f.write('ply\nformat ascii 1.0\n')
    for e in descr:
        count = len(data[e[0]])
        f.write('element ' + e[0] + ' ' + str(count) + '\n')
        for p in e[2]:
            f.write('property ' + ' '.join(p[1]) + ' ' + p[0] + '\n')
    f.write('end_header\n')
    for e in descr:
        edata = data[e[0]]
        for el in edata:
            op = []
            for p in e[2]:
                v = el[p[0]]
                if p[1][0] != 'list':
                    op.append(v)
                else:
                    op.append(len(v))
                    op.extend(v)
            f.write(' '.join(map(str, op)) + '\n')
    f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('init_file')
    args = parser.parse_args()
    path = args.init_file

    counts, data = parse_init(path, verbose=True)

    topology = apply_connective_topology(counts, data)

    print(topology['cells'][0][0])


