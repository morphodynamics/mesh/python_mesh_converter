import logging

import numpy as np

import vtk
from vtk.util.numpy_support import numpy_to_vtk

import pyvista as pv

from converter_py.mesh import Mesh


def unstructured_grid_from_mesh(
    mesh: Mesh, export_edges: bool=True, export_faces: bool=True,
    export_volumes: bool=True
) -> pv.UnstructuredGrid:
    """Create a PyVista (VTK) UnstructuredGrid from a Mesh object.

    The function computes an UnstructuredGrid built upon the points of the
    mesh vertices. It is composed of cells that may comprise Lines for the
    mesh edges, Polygons for the mesh faces and Polyhedra for the mesh volumes.
    All the mesh element properties of convertible types are stored as point
    data or cell data.

    Parameters
    ----------
    mesh:
        The mesh object to convert.
    export_edges:
        Whether to include Line cells in the unstructured grid.
    export_faces:
        Whether to include Polygon cells in the unstructured grid.
    export_volumes:
        Whether to include Polyhedron cells in the unstructured grid.

    Returns
    -------
    grid: pv.UnstructuredGrid
        The generated UnstructuredGrid object

    """
    vertex_point_ids = dict(zip(mesh.vertices.indices, range(len(mesh.vertices.indices))))
    points = [mesh.vertices.points[v] for v in mesh.vertices.indices]

    if mesh.edges.indices is None:
        export_edges = False
    else:
        edge_vertices = [mesh.edges.vertex_indices[e] for e in mesh.edges.indices]

    if mesh.faces.indices is None:
        export_faces = False
    else:
        face_vertices = [mesh.faces.vertex_indices[f] for f in mesh.faces.indices]
        face_vertices_dict = dict(zip(mesh.faces.indices, face_vertices))

    if mesh.volumes.indices is None:
        export_volumes = False
    else:
        volume_oriented_faces = [(mesh.volumes.face_indices[c], mesh.volumes.face_orientations[c]) for c in mesh.volumes.indices]

    cells = []
    cell_types = []
    cell_offsets = [0]

    if export_edges:
        cells += [[vertex_point_ids[vid] for vid in v] for v in edge_vertices]
        cell_types += [pv.CellType.LINE for v in edge_vertices]
        for v in edge_vertices:
            cell_offsets += [cell_offsets[-1] + len(v)]

    if export_faces:
        cells += [[vertex_point_ids[vid] for vid in v] for v in face_vertices]
        cell_types += [pv.CellType.POLYGON for v in face_vertices]
        for v in face_vertices:
            cell_offsets += [cell_offsets[-1] + len(v)]

    if export_volumes:
        cells += [[len(c_f)]+list(np.concatenate([[len(face_vertices_dict[f])] + [vertex_point_ids[vid] for vid in face_vertices_dict[f][::o]] for f, o in zip(c_f, c_o)])) for c_f, c_o in volume_oriented_faces]
        cell_types += [pv.CellType.POLYHEDRON for v in volume_oriented_faces]
        for c_f, c_o in volume_oriented_faces:
            cell_offsets += [cell_offsets[-1] + np.sum([len(face_vertices_dict[f]) + 1 for f in c_f]) + 1]

    if len(cells) > 0:
        cells = np.concatenate(cells)
        cell_types = np.array(cell_types)
        cell_offsets = np.array(cell_offsets)

    vtk_grid = vtk.vtkUnstructuredGrid()
    vtk_coordinates = vtk.vtkPoints()
    vtk_points = numpy_to_vtk(np.array(points, dtype=np.float32))
    vtk_coordinates.SetNumberOfPoints(len(points))
    vtk_coordinates.SetData(vtk_points)
    vtk_grid.SetPoints(vtk_coordinates)

    if len(cells) > 0:
        vtk_cell_type = numpy_to_vtk(cell_types, array_type=vtk.VTK_UNSIGNED_CHAR)
        vtk_cells = numpy_to_vtk(cells, array_type=vtk.VTK_ID_TYPE)
        vtk_offsets = numpy_to_vtk(cell_offsets, array_type=vtk.VTK_ID_TYPE)

        vtk_cell_array = vtk.vtkCellArray()
        vtk_cell_array.SetNumberOfCells(len(cells))
        vtk_cell_array.SetData(vtk_offsets, vtk_cells)
        vtk_grid.SetCells(vtk_cell_type, vtk_cell_array)

    grid = pv.UnstructuredGrid(vtk_grid)

    logging.info("Saving vertex properties")
    for property_name in mesh.vertices.properties:
        logging.info(f"  -> {property_name}")
        vertex_property = np.array([mesh.vertices.properties[property_name][v] for v in mesh.vertices.indices])
        if vertex_property.ndim == 1 and vertex_property.dtype != np.dtype(object): # scalar
            grid.point_data[property_name] = list(vertex_property)
        elif vertex_property.ndim == 2 and vertex_property.shape[1]==3: # vector
            grid.point_data[property_name] = list(vertex_property)
        elif vertex_property.ndim == 3 and vertex_property.shape[1]==3 and vertex_property.shape[2]==3: # tensor
            grid.point_data[property_name] = list(vertex_property)

    logging.info("Saving edge properties")
    if export_edges:
        for property_name in mesh.edges.properties:
            logging.info(f"  -> {property_name}")
            edge_property = np.array([mesh.edges.properties[property_name][e] for e in mesh.edges.indices])
            cell_data = []
            if edge_property.ndim == 1 and edge_property.dtype != np.dtype(object): # scalar
                cell_data += list(edge_property)
                if export_faces:
                    cell_data += [np.nan for _ in mesh.faces.indices]
                if export_volumes:
                    cell_data += [np.nan for _ in mesh.volumes.indices]
                grid.cell_data[property_name] = cell_data
            elif edge_property.ndim == 2 and edge_property.shape[1]==3: # vector
                cell_data += list(edge_property)
                if export_faces:
                    cell_data += [np.tile(np.nan, 3) for _ in mesh.faces.indices]
                if export_volumes:
                    cell_data += [np.tile(np.nan, 3) for _ in mesh.volumes.indices]
                grid.cell_data[property_name] = cell_data
            elif edge_property.ndim == 3 and edge_property.shape[1]==3 and edge_property.shape[2]==3: # tensor
                cell_data += list(edge_property)
                if export_faces:
                    cell_data += [np.tile(np.nan, (3, 3)) for _ in mesh.faces.indices]
                if export_volumes:
                    cell_data += [np.tile(np.nan, (3, 3)) for _ in mesh.volumes.indices]
                grid.cell_data[property_name] = cell_data

    logging.info("Saving face properties")
    if export_faces:
        for property_name in mesh.faces.properties:
            logging.info(f"  -> {property_name}")
            face_property = np.array([mesh.faces.properties[property_name][f] for f in mesh.faces.indices])
            cell_data = []
            if face_property.ndim == 1 and face_property.dtype != np.dtype(object): # scalar
                if export_edges:
                    cell_data += [np.nan for _ in mesh.edges.indices]
                cell_data += list(face_property)
                if export_volumes:
                    cell_data += [np.nan for _ in mesh.volumes.indices]
                grid.cell_data[property_name] = cell_data
            elif face_property.ndim == 2 and face_property.shape[1]==3: # vector
                if export_edges:
                    cell_data += [np.tile(np.nan, 3) for _ in mesh.edges.indices]
                cell_data += list(face_property)
                if export_volumes:
                    cell_data += [np.tile(np.nan, 3) for _ in mesh.volumes.indices]
                grid.cell_data[property_name] = cell_data
            elif face_property.ndim == 3 and face_property.shape[1]==3 and face_property.shape[2]==3: # tensor
                if export_edges:
                    cell_data += [np.tile(np.nan, (3, 3)) for _ in mesh.edges.indices]
                cell_data += list(face_property)
                if export_volumes:
                    cell_data += [np.tile(np.nan, (3, 3)) for _ in mesh.volumes.indices]
                grid.cell_data[property_name] = cell_data

    logging.info("Saving cell properties")
    if export_volumes:
        for property_name in mesh.volumes.properties:
            logging.info(f"  -> {property_name}")
            volume_property = np.array([mesh.volumes.properties[property_name][c] for c in mesh.volumes.indices])
            cell_data = []
            if volume_property.ndim == 1 and volume_property.dtype != np.dtype(object): # scalar
                if export_edges:
                    cell_data += [np.nan for _ in mesh.edges.indices]
                if export_faces:
                    cell_data += [np.nan for _ in mesh.faces.indices]
                cell_data += list(volume_property)
                grid.cell_data[property_name] = cell_data
            elif volume_property.ndim == 2 and volume_property.shape[1]==3: # vector
                if export_edges:
                    cell_data += [np.tile(np.nan, 3) for _ in mesh.edges.indices]
                if export_faces:
                    cell_data += [np.tile(np.nan, 3) for _ in mesh.faces.indices]
                cell_data += list(volume_property)
                grid.cell_data[property_name] = cell_data
            elif volume_property.ndim == 3 and volume_property.shape[1]==3 and volume_property.shape[2]==3: # tensor
                if export_edges:
                    cell_data += [np.tile(np.nan, (3, 3)) for _ in mesh.edges.indices]
                if export_faces:
                    cell_data += [np.tile(np.nan, (3, 3)) for _ in mesh.faces.indices]
                cell_data += list(volume_property)
                grid.cell_data[property_name] = cell_data

    return grid


def write_vtk_mesh(
    filename: str, mesh: Mesh, save_edges: bool=True, save_faces: bool=True,
    save_volumes: bool=True
) -> None:
    """Write a Mesh object as a VTK file.

    The function saves a Mesh object as a .vtk or .vtu file by first converting
    it to an VTK UnstructuredGrid representation. The output file may include
    cells of different types for the mesh edges, faces and volumes.

    Parameters
    ----------
    filename:
        The .vtk or .vtu file to write.
    mesh:
        The mesh object to save
    save_edges:
        Whether to include mesh edges in the saved file.
    save_faces:
        Whether to include mesh faces in the saved file.
    save_volumes:
        Whether to include mesh volumes in the saved file.

    """
    grid = unstructured_grid_from_mesh(
        mesh, export_edges=save_edges, export_faces=save_faces, export_volumes=save_volumes
    )

    if filename.endswith('.vtk'):
        writer = vtk.vtkUnstructuredGridWriter()
        writer.SetFileTypeToASCII()
    elif filename.endswith('.vtu'):
        writer = vtk.vtkXMLUnstructuredGridWriter()
        writer.SetDataModeToAscii()
    else:
        raise ValueError("The extension should either be .vtk or .vtu")
    writer.SetFileName(filename)
    writer.SetInputData(grid)
    writer.Update()
