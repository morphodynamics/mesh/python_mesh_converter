

"""
Usage: python vtk_view.py infile.ply outfile.vtk
"""


import vtk
import numpy as np
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, numpy_to_vtkIdTypeArray

from ply_parser_string import parse_ply

import sys


def mkVtkIdList(it):
    vil = vtk.vtkIdList()
    for i in it:
        vil.InsertNextId(int(i))
    return vil

res = parse_ply(sys.argv[1])
print(res[1]['vertex'][0])
print(res[1]['face'][0])
#x = res[1]['vertex'][0]


vert_labels = []
verts = []
for v in res[1]['vertex']:
    verts.append([v['x'], v['y'], v['z']])
    vert_labels.append(v.get('label', 1))

vert_labels = np.array(vert_labels, dtype=float)

verts = np.array(verts, dtype=np.float64)

#verts = verts - np.mean(verts, axis=0)
#print(verts)
#verts = verts/np.max(np.abs(verts))


faces = []
face_labels = []

for f in res[1]['face']:
    faces.append(f['vertex_index'])
    face_labels.append(f.get('label', 1))

face_labels = np.array(face_labels, dtype=float)
print('face_labels', np.min(face_labels), np.max(face_labels))

ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
 
# create a renderwindowinteractor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
 
# create points
points = vtk.vtkPoints()


points.SetData(numpy_to_vtk(verts))


cells = vtk.vtkCellArray() 

triangles = vtk.vtkCellArray()
for p in faces:
    triangles.InsertNextCell(mkVtkIdList(p))


trianglePolyData = vtk.vtkPolyData()
trianglePolyData.SetPoints( points )
#del points
trianglePolyData.SetPolys( triangles )
#trianglePolyData.GetPointData().SetScalars(numpy_to_vtk(vert_labels))
trianglePolyData.GetCellData().SetScalars(numpy_to_vtk(face_labels))

writer = vtk.vtkPolyDataWriter()
writer.SetFileName(sys.argv[2])
writer.SetInputData(trianglePolyData)
writer.Write()
 
# mapper
mapper = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
    mapper.SetInput(trianglePolyData)
else:
    mapper.SetInputData(trianglePolyData)

mapper.SetScalarRange(0, 10000) 
# actor
actor = vtk.vtkActor()
actor.SetMapper(mapper)
 
# assign actor to the renderer
ren.AddActor(actor)

camera = vtk.vtkCamera()
#camera.SetPosition(100,100,100)
#camera.SetFocalPoint(0,0,0)
 


ren.SetActiveCamera(camera)
ren.ResetCamera()
ren.SetBackground(0,0,0)
 
renWin.SetSize(300,300)

# enable user interface interactor
iren.Initialize()
renWin.Render()
iren.Start()
