import logging
from typing import Any, Dict, Iterable, List, Optional
from numbers import Real, Integral as Integer


class ElementProperty(dict):
    """
    Container representing a property defined over mesh elements of the same type.

    The structure is a dictionary whose keys are the indices of the elements
    and the values the property values. It also stores the name of the property
    as well as its type, that should match the type of the dictionary values.

    """
    property_name: str
    property_type: List[str]

    def __init__(self, property_name, property_type):
        super().__init__()
        self.property_name = property_name
        self.property_type = property_type

    @staticmethod
    def guess_type(value):
        if isinstance(value, Real):
            return ['float']
        elif isinstance(value, Integer):
            return ['int']
        elif isinstance(value, Iterable):
            if len(value) > 0:
                if isinstance(value[0], Real):
                    return ['list', 'int', 'float']
                elif isinstance(value[0], Integer):
                    return ['list', 'int', 'int']
            else:
                return ['list', 'int', 'int']
        else:
            logging.error(f"Unsupported type {type(value)}!")


class MeshElements(object):
    """
    Generic container for mesh elements carrying properties.

    Elements store a name that describes the type of element they represent,
    a list of integer indices identifying the elements, and a dictionary of
    named properties associated to the element indices.

    """
    element_name: str
    indices: List[int]
    properties: Dict[str, ElementProperty]

    def __init__(self):
        self.element_name = "elements"
        self.indices = None
        self.properties = {}

    def __repr__(self):
        repr_str = f"<class {self.__class__.__name__} "
        repr_str += f"with {len(self.indices) if self.indices is not None else 'no'} {self.element_name} ["
        if len(self.properties) > 0:
            for i_p, property_name in enumerate(self.properties):
                repr_str += f"{property_name}{', ' if i_p < len(self.properties)-1 else ''}"
        else:
            repr_str += "no"
        repr_str += f" propert{'y' if len(self.properties)==1 else 'ies'}]>"
        return repr_str

    def __len__(self):
        return len(self.indices) if self.indices is not None else 0


class VertexElements(MeshElements):
    """
    Specific container for mesh vertices.

    In addition to indices and properties, vertex elements store their positions
    as a dictionary whose keys are the vertex indices and values the associated
    2D or 3D points.
    """

    points: Dict[int, List[float]]

    def __init__(self):
        super().__init__()
        self.points = None
        self.element_name = "vertices"


class EdgeElements(MeshElements):
    """
    Specific container for mesh edges.

    In addition to indices and properties, edge elements store the indices
    of their vertices as a dictionary whose keys are the edge indices and
    values the associated 2-tuple of vertex indices.
    """
    vertex_indices: Dict[int, List[int]]

    def __init__(self):
        super().__init__()
        self.vertex_indices = None
        self.element_name = "edges"


class FaceElements(MeshElements):
    """
    Specific container for mesh faces.

    In addition to indices and properties, face elements store the indices
    of their vertices as a dictionary whose keys are the edge indices and
    values the associated n-tuple of correctly ordered vertex indices.
    """
    vertex_indices: Dict[int, List[int]]

    def __init__(self):
        super().__init__()
        self.vertex_indices = None
        self.element_name = "faces"


class VolumeElements(MeshElements):
    """
    Specific container for mesh volumes.

    In addition to indices and properties, volume elements store the indices
    of their faces as a dictionary whose keys are the volume indices and
    values the associated n-tuple correctly ordered face indices, as well
    as the orientations of their faces as a dictionary whose keys are the
    volume indices and values the n-tuple assigning a -1 or +1 orientation
    to each face.
    """
    face_indices: Dict[int, List[int]]
    face_orientations: Dict[int, List[int]]

    def __init__(self):
        super().__init__()
        self.face_indices = None
        self.face_orientations = None
        self.element_name = "volumes"


element_classes = {
    'vertex': VertexElements,
    'edge': EdgeElements,
    'face': FaceElements,
    'volume': VolumeElements
}


class Mesh(dict):
    """
    Structure representing the elements of a tissue mesh.

    The structure provides the minimal information to reconstruct a tissue mesh
    composed of vertex, edge, face and volume elements. It contains the
    topological relationships between the different types of elements, along
    with any kind of property attached to them, but does not provide any actual
    processing functionality.

    It is aimed to be used as a bridging facility between actual mesh data
    structures, for the conversion of mesh file formats or for visualization
    purposes.

    Parameters
    ----------
    vertex_points
        3D positions of the mesh vertices
    vertex_indices
        Integer indices for the mesh vertices
    face_vertex_indices
        For each face, list of indices of its vertices
    face_indices
        Integer indices for the mesh faces
    edge_vertex_indices
        For each edge, list of indices of its vertices
    volume_face_indices
        For each volume, list of indices of its faces
    volume_face_orientations
        For each volume, list of relative orientation (-1, 1) of its faces

    """

    vertices: VertexElements
    edges: EdgeElements
    faces: FaceElements
    volumes: VolumeElements
    metadata: Dict

    def __init__(
        self, vertex_points: List[List[float]]=[],
        vertex_indices: Optional[List[int]]=None,
        face_vertex_indices: List[List[int]]=[],
        face_indices: Optional[List[int]]=None,
        edge_vertex_indices: List[List[int]]=[],
        volume_face_indices: List[List[int]]=[],
        volume_face_orientations: Optional[List[List[int]]]=None
    ):
        
        super().__init__()

        for element_name, element_class in element_classes.items():
            self[element_name] = element_class()
        self.metadata = {}

        self._init_vertices(vertex_points, vertex_indices)
        self._init_faces(face_vertex_indices, face_indices)
        self._init_edges(edge_vertex_indices)
        self._init_volumes(volume_face_indices, volume_face_orientations)

    def __setitem__(self, key, value):
        if key in element_classes:
            if isinstance(value, element_classes[key]):
                super().__setitem__(key, value)
            else:
                logging.warning(f"Unsupported value for key {key}, no change to be done")
        else:
            logging.warning(f"Unsupported key {key}, no change to be done")

    def __delitem__(self, key):
        logging.warning(f"Impossible to delete key {key}, no change to be done")

    def _init_vertices(self, vertex_points, vertex_indices=None):
        if len(vertex_points) > 0:
            if vertex_indices is None:
                vertex_indices = list(range(len(vertex_points)))
            assert len(vertex_points) == len(vertex_indices)
            self.vertices.indices = vertex_indices
            self.vertices.points = dict(zip(vertex_indices, vertex_points))

    def _init_edges(self, edge_vertex_indices):
        if len(edge_vertex_indices) > 0:
            edge_indices = list(range(len(edge_vertex_indices)))
            self.edges.indices = edge_indices
            self.edges.vertex_indices = dict(zip(edge_indices, edge_vertex_indices))

    def _init_faces(self, face_vertex_indices, face_indices=None):
        if len(face_vertex_indices) > 0:
            if face_indices is None:
                face_indices = list(range(len(face_vertex_indices)))
            assert len(face_vertex_indices) == len(face_indices)
            self.faces.indices = face_indices
            self.faces.vertex_indices = dict(zip(face_indices, face_vertex_indices))

    def _init_volumes(self, volume_face_indices, volume_face_orientations):
        if len(volume_face_indices) > 0:
            volume_indices = list(range(len(volume_face_indices)))
            self.volumes.indices = volume_indices
            self.volumes.face_indices = dict(zip(volume_indices, volume_face_indices))
            if volume_face_orientations is None:
                volume_face_orientations = [[1 for _ in f] for f in volume_face_indices]
            self.volumes.face_orientations = dict(zip(volume_indices, volume_face_orientations))

    def set_property(
        self, element_name: str, property_name: str, property_dict: Dict[int, Any],
        property_type: Optional[List[str]]=None
    ) -> None:
        """Set a property for a given element type.

        Parameters
        ----------
        element_name
            Name of the elements on which to set the property
        property_name
            Name of the property
        property_dict
            For each element index, the value of the property
        property_type
            Stringified type of the property

        """
        if element_name in self:
            assert all([elmt_id in property_dict for elmt_id in self[element_name].indices])
            if property_type is None:
                property_type = ElementProperty.guess_type(list(property_dict.values())[0])
            self[element_name].properties[property_name] = ElementProperty(property_name, property_type)
            self[element_name].properties[property_name].update({
                elmt_id: property_dict[elmt_id] for elmt_id in self[element_name].indices
            })
        else:
            logging.error(f"Non-existing element type {element_name}!")

    @property
    def vertices(self):
        return self['vertex']

    @property
    def edges(self):
        return self['edge']

    @property
    def faces(self):
        return self['face']

    @property
    def volumes(self):
        return self['volume']
