import init_parser as ip


class Tissue(object):
    def __init__(self, path):
        self._path = path

        self._cells = []
        self._vertices = []
        self._walls = []

        self._counts = []
        self._data = []
        self._counts, self._data = self._tissue_from_init(self._path)

        self._n_cells = int(self._counts['cells'])
        self._n_walls = int(self._counts['walls'])
        self._n_vertices = int(self._counts['vertices'])

        self._populate_tissue()
        self._connect_tissue()

        # TODO Fix assertions
        # assert len(self._cells) == self._n_cells
        # assert len(self._vertices) == self._n_vertices
        # assert len(self._walls) == self._n_walls

    def cells(self):
        for _cell in self._cells:
            yield _cell

    def vertices(self):
        for _vertex in self._vertices:
            yield _vertex

    def walls(self):
        for _wall in self._walls:
            yield _wall

    @staticmethod
    def _tissue_from_init(_path):
        return ip.parse_init(_path, verbose=False)

    def _populate_tissue(self):
        for i, cell in enumerate(self._data['cells']):
            temp_cell = Cell(i, cell)
            self._cells.append(temp_cell)
        for i, _vertex in enumerate(self._data['vertices']):
            temp_vertex = Vertex(i, _vertex[0], _vertex[1])
            self._vertices.append(temp_vertex)
        for i, wall in enumerate(self._data['walls']):
            temp_wall = Wall(i)
            self._walls.append(temp_wall)

    def _connect_tissue(self):
        # TODO: THIS!!
        pass


class Cell(object):
    def __init__(self, cell_id, var_list):
        self._cell_id = cell_id
        self._variables = var_list
        self._walls = []
        self._vertices = []

    def __str__(self):
        return "I am cell " + str(self._cell_id)

    def add_wall(self, _wall_id):
        self._walls.append(_wall_id)

    def add_vertex(self, _vertex_id):
        self._vertices.append(_vertex_id)


class Wall(object):
    def __init__(self, wall_id):
        self._wall_id = wall_id
        self._vertices = []
        self._cells = []

    def __str__(self):
        return "I am Wall " + str(self._wall_id)

    def add_vertex(self, _vertex_id):
        self._vertices.append(_vertex_id)

    def add_cell(self, _cell_id):
        self._cells.append(_cell_id)


class Vertex(object):
    def __init__(self, vertex_id, x, y):
        self._vertex_id = vertex_id
        self._x = float(x)
        self._y = float(y)
        self._walls = []
        self._cells = []

    def __str__(self):
        return "I am vertex " + str(self._vertex_id) + \
               " at: (x,y) " + str(self._x) + ", " + str(self._y)

    def add_cell(self, _cell_id):
        self._cells.append(_cell_id)

    def add_wall(self, _wall_id):
        self._walls.append(_wall_id)

    def get_position(self):
        return self._x, self._y


if __name__ == "__main__":
    init_path = "/Volumes/data/tissue_projects/kanrev_model/with_kanrev_out.init"
    tissue = Tissue(init_path)

    import matplotlib.pyplot as plt
    for vertex in tissue.vertices():
        plt.plot(*vertex.get_position(), marker='.', ms=6)

    plt.show()
