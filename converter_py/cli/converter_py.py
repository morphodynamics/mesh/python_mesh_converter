#!/usr/bin/env python
import os
import argparse

import converter_py.init_parser as ip
import converter_py.ply_parser as pp


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('output_file')

    args = parser.parse_args()

    print('input is: ', args.input_file)

    # Import
    # ply
    ply_desc, ply_data = None, None
    if args.input_file.endswith('.ply'):
        print('input is .ply format')
        ply_desc, ply_data = pp.parse_ply(args.input_file)

    if args.input_file.endswith('.init'):
        print('input is .init format')
        init_counts, init_data = ip.parse_init(args.input_file)

    # numpy fudging

    # Export
    # check if output exists
    if os.path.exists(args.output_file):
        option = raw_input('output file already exists, overwrite? (y/n): ')
        if option == 'y':
            pass
        elif option == 'n':
            print('exiting')
            return
        else:
            print('try again with (y/n)')

    print('output is: ', args.output_file)

    # Export Init
    if args.output_file.endswith('.init'):
        print('output is .init format')
        pass

    # Export Ply
    elif args.output_file.endswith('.ply'):
        print('output is .ply format')
        pp.write_ply(args.output_file, ply_desc, ply_data)


if __name__ == '__main__':
    main()
