"""original code courtesy of john fozard"""
import argparse
from typing import Dict, List, Optional, Tuple

import numpy as np

from .mesh import Mesh


def parse_ply(filename: str, return_comments: bool=False) -> Tuple[List[Tuple[str, int, List]], Dict[str, List[Dict]], Optional[List[str]]]:
    """Extract element info and data from a PLY tissue mesh file.

    Parameters
    ----------
    filename:
        Path to the PLY filename to read
    return_comments:
        Whether to return the header comments

    Returns
    -------
    element_list: List[Tuple[str, int, List]]
        Name, count and typed property list of each element type
    element_data: Dict[str, List[Dict]]
        List of elements represented by a dictionary of their properties for
        each element type
    comments: Optional[List[str]]
        A list of comments as they appear in the file header


    """
    f = open(filename, 'r')
    #
    comments = []
    l = f.readline()
    if l[0:3] != 'ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0] != 'format' or sl[1] != 'ascii':
        raise RuntimeError('File not in ascii format')
    l = f.readline()
    while l.split()[0] == 'comment':
        comments += [l[l.index(' ') + 1:]]
        print(l[l.index(' ') + 1:])
        l = f.readline()
    element_list = []
    while l.strip() != 'end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0] == 'property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    element_data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]
        eltype_data = []
        for i in range(el[1]):
            el_data = {}
            l = f.readline()
            sl = l.split()
            buf = sl
            for prop in el[2]:
                prop_name = prop[0]
                prop_type = prop[1]
                if prop_type[0] == 'list':
                    count = int(buf[0])
                    if prop_type[2] == 'float' or prop_type[2] == 'double':
                        prop_data = list(map(float, buf[1: count + 1]))
                    else:
                        prop_data = list(map(int, buf[1: count + 1]))
                    buf = buf[count + 1:]
                else:
                    if prop_type[0] == 'float' or prop_type[0] == 'double':
                        prop_data = float(buf[0])
                    else:
                        prop_data = int(buf[0])
                    buf = buf[1:]
                el_data[prop_name] = prop_data
            eltype_data.append(el_data)
        element_data[eltype_name] = eltype_data
    if return_comments:
        return element_list, element_data, comments
    else:
        return element_list, element_data


def write_ply(filename, descr, data):
    f = open(filename, 'w')
    f.write('ply\nformat ascii 1.0\n')
    for e in descr:
        count = len(data[e[0]])
        f.write('element ' + e[0] + ' ' + str(count) + '\n')
        for p in e[2]:
            f.write('property ' + ' '.join(p[1]) + ' ' + p[0] + '\n')
    f.write('end_header\n')
    for e in descr:
        edata = data[e[0]]
        for el in edata:
            op = []
            for p in e[2]:
                v = el[p[0]]
                if p[1][0] != 'list':
                    op.append(v)
                else:
                    op.append(len(v))
                    op.extend(v)
            f.write(' '.join(list(map(str, op))) + '\n')
    f.close()


def read_ply_mesh(filename: str) -> Mesh:
    """Read a PLY formatted tissue mesh file.

    The functions reads the information stored in a PLY mesh file and stores
    all the information (xyz positions, topological relationships, element
    properties) into a Mesh object.

    Parameters
    ----------
    filename:
        Path to the PLY file to read

    Returns
    -------
    mesh: Mesh
        Data structure containing the information of all elements.

    """

    element_list, element_data, comments = parse_ply(filename, return_comments=True)
    element_names = [e[0] for e in element_list]
    element_properties = {e[0]: dict(e[2]) for e in element_list}

    points = []
    if 'vertex' in element_names:
        points = [[v_d[p] for p in 'xyz'] for v_d in element_data['vertex']]
        for p in 'xyz':
            del element_properties['vertex'][p]

    edges = []
    if 'edge' in element_names:
        edges = [[e_d[p] for p in ['source', 'target']] for e_d in element_data['edge']]
        for p in ['source', 'target']:
            del element_properties['edge'][p]

    faces = []
    if 'face' in element_names:
        faces = [f_d['vertex_index'] for f_d in element_data['face']]
        for p in ['vertex_index']:
            del element_properties['face'][p]

    volumes = []
    volume_orientations = []
    if 'volume' in element_names:
        oriented_faces = any([f<0 for c_d in element_data['volume'] for f in c_d['face_index']])
        volumes = [[abs(f)-oriented_faces for f in c_d['face_index']] for c_d in element_data['volume']]
        volume_orientations = [[np.sign(f) if oriented_faces else 1 for f in c_d['face_index']] for c_d in element_data['volume']]
        for p in ['face_index']:
            del element_properties['volume'][p]

    mesh = Mesh(
        vertex_points=points, face_vertex_indices=faces, edge_vertex_indices=edges,
        volume_face_indices=volumes, volume_face_orientations=volume_orientations
    )

    for element_name in element_names:
        for property_name in element_properties[element_name]:
            property_dict = {
                c: c_d[property_name]
                for c, c_d in zip(mesh[element_name].indices, element_data[element_name])
            }
            mesh.set_property(
                element_name, property_name, property_dict,
                element_properties[element_name][property_name]
            )

    mesh.metadata['comments'] = comments

    return mesh


def write_ply_mesh(filename: str, mesh: Mesh) -> None:
    """Write a PLY formatted tissue mesh file.

    Parameters
    ----------
    filename:
        Path where to write the PLY file
    mesh:
        The tissue mesh structure to write

    """
    element_names = [
        name for name in ['vertex', 'face', 'edge', 'volume']
        if name in mesh
        and mesh[name].indices is not None
    ]

    f = open(filename, 'w')
    f.write('ply\nformat ascii 1.0\n')
    if 'comments' in mesh.metadata:
        for comment in mesh.metadata['comments']:
            f.write(f"comment {comment}")
            if not comment.endswith('\n'):
                f.write("\n")
    f.write(f"comment Converted using converter_py\n")
    for element_name in element_names:
        elements = mesh[element_name]
        count = len(elements.indices)
        f.write(f"element {element_name} {count}\n")
        if element_name in ['vertex']:
            f.write('property float x\nproperty float y\nproperty float z\n')
        elif element_name in ['edge']:
            f.write('property int source\nproperty int target\n')
        elif element_name in ['face']:
            f.write('property list uchar uint vertex_index\n')
        elif element_name in ['volume']:
            f.write('property list uchar int face_index\n')
        for property_name, element_property in elements.properties.items():
            property_type = element_property.property_type
            f.write(f"property {' '.join(property_type)} {property_name}\n")
    f.write('end_header\n')
    for element_name in element_names:
        elements = mesh[element_name]
        for e in elements.indices:
            element_properties = []
            if element_name in ['vertex']:
                element_properties += list(elements.points[e])
            elif element_name in ['edge']:
                element_properties += list(elements.vertex_indices[e])
            elif element_name in ['face']:
                element_properties += [len(elements.vertex_indices[e])] + list(elements.vertex_indices[e])
            elif element_name in ['volume']:
                element_properties += [len(elements.face_indices[e])] + [o*(f+1) for f, o in zip(elements.face_indices[e], elements.face_orientations[e])]
            for property_name, element_property in elements.properties.items():
                property_type = element_property.property_type
                property_value = elements.properties[property_name][e]
                if property_type[0] != 'list':
                    element_properties.append(property_value)
                else:
                    element_properties.append(len(property_value))
                    element_properties.extend(property_value)
            f.write(f"{' '.join(list(map(str, element_properties)))}\n")
    f.close()


def ply_to_init(element_list, data):
    print(element_list)

    vertices = []
    for vertex in data['vertex']:
        vertices.append([vertex['x'], vertex['y'], vertex['z']])

    init_counts = {'vertices': len(vertices)}
    init_data = {'vertices': vertices}
    return init_counts, init_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('ply_file')
    args = parser.parse_args()

    # path = '../example_data/segmentation_topomesh.ply'
    path = args.ply_file
    element_list, data = parse_ply(path)
    init_counts, init_data = ply_to_init(element_list, data)

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for i, vertex in enumerate(init_data['vertices']):
        ax.scatter(vertex[0], vertex[1], vertex[2], 'o', s=30, alpha=0.3)

    # ax.plot([2, 1, 2], [3, 1, 5], [1, 4, 6])
    plt.show()
