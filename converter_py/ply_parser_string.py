"""original code courtesy of john fozard"""
import argparse



def split_line(l):
    if '"' not in l:
        return l.split()
    else:
        w = l.split('"')
        w2 = sum([u.split() if i%2==0 else [u] for i,u in enumerate(w)], [])
        return w2

def parse_ply(filename):
    f = open(filename, 'r')
    #
    l = f.readline()
    if l[0:3] != 'ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0] != 'format' or sl[1] != 'ascii':
        raise RuntimeError('File not in ascii format')
    l = f.readline()
    while l.split()[0] == 'comment':
        print(l[l.index(' ') + 1:])
        l = f.readline()
    element_list = []
    while l.strip() != 'end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0] == 'property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]
        eltype_data = []
        for i in range(el[1]):
            el_data = {}
            l = f.readline()
            sl = split_line(l)
            buf = sl
            for prop in el[2]:
                prop_name = prop[0]
                prop_type = prop[1]
                if prop_type[0] == 'list':
                    count = int(buf[0])
                    if prop_type[2] == 'float' or prop_type[2] == 'double':
                        prop_data = map(float, buf[1: count + 1])
                    else:
                        prop_data = map(int, buf[1: count + 1])
                    buf = buf[count + 1:]
                elif prop_type[0] == 'string':
                    prop_data = str(buf[0])
                else:
                    if prop_type[0] == 'float' or prop_type[0] == 'double':
                        prop_data = float(buf[0])
                    else:
                        prop_data = int(buf[0])
                    buf = buf[1:]
                el_data[prop_name] = prop_data
            eltype_data.append(el_data)
        data[eltype_name] = eltype_data
    return element_list, data


def write_ply(filename, descr, data):
    f = open(filename, 'w')
    f.write('ply\nformat ascii 1.0\n')
    for e in descr:
        count = len(data[e[0]])
        f.write('element ' + e[0] + ' ' + str(count) + '\n')
        for p in e[2]:
            f.write('property ' + ' '.join(p[1]) + ' ' + p[0] + '\n')
    f.write('end_header\n')
    for e in descr:
        edata = data[e[0]]
        for el in edata:
            op = []
            for p in e[2]:
                v = el[p[0]]
                if p[1][0] == 'list':
                    op.append(len(v))
                    op.extend(v)
                elif p[1][0] == 'string':
                    s = '"'+v+'"'
                    op.append(s)
                else:
                    op.append(v)
            f.write(' '.join(map(str, op)) + '\n')
    f.close()


def ply_to_init(element_list, data):
    print(element_list)

    vertices = []
    for vertex in data['vertex']:
        vertices.append([vertex['x'], vertex['y'], vertex['z']])

    init_counts = {'vertices': len(vertices)}
    init_data = {'vertices': vertices}
    return init_counts, init_data

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('ply_file')
    args = parser.parse_args()

    # path = '../example_data/segmentation_topomesh.ply'
    path = args.ply_file
    element_list, data = parse_ply(path)
    init_counts, init_data = ply_to_init(element_list, data)

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for i, vertex in enumerate(init_data['vertices']):
        ax.scatter(vertex[0], vertex[1], vertex[2], 'o', s=30, alpha=0.3)

    # ax.plot([2, 1, 2], [3, 1, 5], [1, 4, 6])
    plt.show()
