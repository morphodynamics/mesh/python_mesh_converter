
from ply_parser import *


def main():

    descr = [('vertex', 2, [('x', ['float']), ('y', ['float']), ('z', ['float']) ]), ('face', 1, [('vertex_index', ['list', 'int', 'int'] ) ]) ]
    data = {'vertex': [ [[1,2,3], [2,3,4] ] ],
            'face' : [ [[[0, 1]]] ] }

    write_ply_binary('test_output.ply', descr, data)

    new_descr, new_data = parse_ply('test_output.ply')

    print('CHECK HEADER', descr == new_descr)

    print(dict((el, zip(*d[0])) for el, d in new_data.iteritems()))
    print(data)


if __name__=="__main__":
    main()
