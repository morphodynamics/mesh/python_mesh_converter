"""
Very simple functions to help with reading and writing
ASCII format PLY files
Author: John Fozard
"""

import numpy as np

from gzip import GzipFile
import struct


def open_opt_compress(filename, mode):
    """
    Open a filename, with optional gzip compression

    Args:
       filename (str) - filename and path
       mode - as for standard python open ("r" = read, "w" = write etc)

    Returns:
       file object
    """

    if len(filename) > 3 and filename[-3:] == '.gz':
        return GzipFile(str(filename), mode)
    else:
        return open(filename, mode)


def parse_ply_ascii(f):
    l = f.readline()
    while l.split()[0] == 'comment':
        print(l[l.index(' ') + 1:])
        l = f.readline()
    element_list = []
    while l.strip() != 'end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0] == 'property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]

        prop_types = []
        for prop in el[2]:
            prop_type = prop[1]
            if prop_type[0] == 'list':
                if prop_type[2] == 'float' or prop_type[2] == 'double':
                    prop_types.append((True, float))
                else:
                    prop_types.append((True, int))
            else:
                if prop_type[0] == 'float' or prop_type[0] == 'double':
                    prop_types.append((False, float))
                else:
                    prop_types.append((False, int))

        eltype_data = [[] for _ in prop_types]

        for j in range(eltype_count):
            buf = f.readline().split()
            i = 0
            for k, (l, t) in enumerate(prop_types):
                if l:
                    count = int(buf[i])
                    eltype_data[k].append(map(t, buf[i + 1:i + 1 + count]))
                    i += count + 1
                else:
                    eltype_data[k].append(t(buf[i]))
                    i += 1

        data[eltype_name] = (eltype_data, [x[0] for x in el[2]])
    print(data['face'][-1])

    print('done_parse')
    return element_list, data


map_type = {'char': ('b', 1),
            'uchar': ('B', 1),
            'short': ('h', 2),
            'ushort': ('H', 2),
            'int': ('i', 4),
            'uint': ('I', 4),
            'float': ('f', 4),
            'double': ('d', 8),
            'int8': ('b', 1),
            'uint8': ('B', 1),
            'int16': ('h', 2),
            'uint16': ('H', 2),
            'int32': ('i', 4),
            'uint32': ('I', 4),
            'float32': ('f', 4),
            'float64': ('d', 8)}


def read_binary(f, t, endian):
    """
    Helper function to read a single element from a binary file
    See struct.unpack for more details

    Args:
       f - file handle
       t (str) - name of type
       endian (char) '<'= little endian '>' = big endian (for objects >1 byte in size 
    Returns:
       single object read from file

    """

    type_string, type_size = map_type[t]
    return struct.unpack(endian + type_string, f.read(type_size))[0]


def write_binary(f, v, t, endian='<'):
    """
    Helper function to write a single element to a binary file
    See struct.pack for more details

    Args:
       f - file handle
       v - value to write
       t (str) - name of type
       endian (char) '<'= little endian '>' = big endian (for objects >1 byte in size 
    """

    type_string, type_size = map_type[t]
    f.write(struct.pack(endian + type_string, v))


def parse_ply_binary(f, endian):
    l = f.readline()
    while l.split()[0] == 'comment':
        print(l[l.index(' ') + 1:])
        l = f.readline()
    element_list = []
    while l.strip() != 'end_header':
        el_name = l.split()[1]
        el_count = int(l.split()[2])
        property_list = []
        l = f.readline()
        sl = l.split()
        while sl[0] == 'property':
            prop_name = sl[-1]
            prop_type = l.split()[1:-1]
            property_list.append((prop_name, prop_type))
            l = f.readline()
            sl = l.split()
        element_list.append((el_name, el_count, property_list))
    data = {}
    for el in element_list:
        eltype_name = el[0]
        eltype_count = el[1]

        prop_types = []
        binary_prop_types = []

        for prop in el[2]:
            prop_type = prop[1]
            if prop_type[0] == 'list':
                if prop_type[2] == 'float' or prop_type[2] == 'double':
                    prop_types.append((True, float))
                else:
                    prop_types.append((True, int))
                binary_prop_types.append((True, prop_type[1], prop_type[2]))

            else:
                if prop_type[0] == 'float' or prop_type[0] == 'double':
                    prop_types.append((False, float))
                else:
                    prop_types.append((False, int))
                binary_prop_types.append((False, None, prop_type[0]))

        eltype_data = [[] for _ in prop_types]

        for j in range(eltype_count):
            for k, (l, ct, t) in enumerate(binary_prop_types):
                if l:
                    count = read_binary(f, ct, endian)
                    eltype_data[k].append([read_binary(f, t, endian) for i in range(count)])
                else:
                    eltype_data[k].append(read_binary(f, t, endian))

        data[eltype_name] = (eltype_data, [x[0] for x in el[2]])
    print(data['face'][-1])

    print('done_parse')
    return element_list, data


def parse_ply(filename):
    """
    Parse a PLY Mesh file

    Args:
       filename (str) - filename (with path if not in cwd) of file to open

    Returns:
       element_list (list), data (dict)

    element_list is list of element types (from parsing PLY header).

    From a typical PLY header:

    [('vertex', 1932, [('x', ['float']), ('y', ['float']), ('z', ['float'])]),      ('face', 2281, [('vertex_index', ['list', 'int', 'int']), ('red', ['uchar#]), ('green', ['uchar']), ('blue', ['uchar'])])]    

    List of (element_name, element_count, property_list) tuples.

    The property_list is a list of properties, which are 
    (property_name, property_type) tuples.

    The property_type is a list, either with a single element for scalar
    types (e.g. ['float']), or three elements for list types 
    (e.g. ['list', 'uchar', 'float']), where the second entry ('uchar') is used
    for the list length (needs to be stored in the PLY file for binary types)
    and the third entry is the type of the list elements.
    
    data is a dictionary mapping element names to property_lists
    (e.g. data = {'vertex': [[[1,3], [2,4], [3,5]], ['x','y','z']]})

    property_list[0] is a list of element properties.
      Each element of this is either a list (for scalar properties),
      or a lists of list (list properties).

    property_list[1] is a repeat of the property names. 
       
    """

    f = open_opt_compress(filename, 'rb')
    #
    l = f.readline()
    if l[0:3] != 'ply':
        raise RuntimeError('No ply magic string at beginning of file')
    l = f.readline()
    sl = l.split()
    if sl[0] != 'format':
        raise RuntimeError('No format line')
    if sl[1] == 'ascii':
        return parse_ply_ascii(f)
    elif sl[1] == 'binary_little_endian':
        return parse_ply_binary(f, '<')
    elif sl[1] == 'binary_big_endian':
        return parse_ply_binary(f, '>')
    else:
        raise RuntimeError('Unknown format type' + sl[1])


def write_ply(filename, descr, data):
    """ 
    Write a mesh to an ASCII PLY file

    Args:
        filename (str) - filename (with path if necessary)
        descr - description of element data (same format as element_list
                in load_ply)

        data - data for each element. This is not in the same format 
               as the output of parse_ply.
    
               Instead, I think it looks like:
               { 'vertex': [ [[1,2,3],[3,4,5]] ] }
               i.e. a list of the data for each element, where the data for
               each element is written in the same order as in descr.
               
    """

    f = open_opt_compress(filename, 'w')
    f.write('ply\nformat ascii 1.0\ncomment mesh project\n')
    for e in descr:
        count = len(data[e[0]][0])
        f.write('element ' + e[0] + ' ' + str(count) + '\n')
        for p in e[2]:
            f.write('property ' + ' '.join(p[1]) + ' ' + p[0] + '\n')
    f.write('end_header\n')
    for e in descr:
        edata = data[e[0]][0]
        for el in edata:
            # print(el)
            op = []
            for i, p in enumerate(e[2]):
                v = el[i]
                # print(v, type(v), p[1][0])
                if p[1][0] != 'list':
                    op.append(v)
                else:
                    op.append(len(v))
                    op.extend(v)
            f.write(' '.join(map(str, op)) + '\n')
    f.close()


def write_ply_binary(filename, descr, data):
    """ Write a mesh to an binary PLY file
    """
    f = open_opt_compress(filename, 'wb')
    f.write('ply\nformat binary_little_endian 1.0\ncomment mesh project\n')
    for e in descr:
        count = len(data[e[0]][0])
        f.write('element ' + e[0] + ' ' + str(count) + '\n')
        for p in e[2]:
            f.write('property ' + ' '.join(p[1]) + ' ' + p[0] + '\n')
    f.write('end_header\n')
    for e in descr:
        edata = data[e[0]][0]
        for el in edata:
            op = []
            for i, p in enumerate(e[2]):
                v = el[i]
                if p[1][0] != 'list':
                    write_binary(f, v, p[1][0])
                else:
                    write_binary(f, len(v), p[1][1])
                    for u in v:
                        write_binary(f, u, p[1][2])
    f.close()
