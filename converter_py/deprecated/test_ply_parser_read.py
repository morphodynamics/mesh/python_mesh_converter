
from ply_parser import *
import sys

def main():

    descr, data = parse_ply(sys.argv[1])

    print(descr)
#    print(data)
    new_data = dict((el, [zip(*d[0])]) for el, d in data.iteritems())

#    print(new_data)

    write_ply(sys.argv[2], descr, new_data)
    

if __name__=="__main__":
    main()
