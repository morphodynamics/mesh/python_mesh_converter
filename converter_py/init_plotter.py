import os
import argparse

import matplotlib
import matplotlib.pyplot as plt

import init_parser as ip


def plot_init(counts_, data_):
    for vertex in data_['vertices']:
        plt.plot(vertex[0], vertex[1], '.', ms=6)

    wall_data = []
    wall_data_ind = 3
    for wall in data_['walls_info']:
        wall_data.append(float(wall[wall_data_ind]))
    min_l, max_l = min(wall_data), max(wall_data)

    color_map = matplotlib.cm.ScalarMappable(cmap='viridis')
    color_map.set_clim(vmin=min_l, vmax=max_l)

    for j, wall in enumerate(data_['walls']):
        vert_from, vert_to = int(wall[3]), int(wall[4])
        color_from_data = color_map.to_rgba(wall_data[j])
        plt.plot([data_['vertices'][vert_from][0], data_['vertices'][vert_to][0]],
                 [data_['vertices'][vert_from][1], data_['vertices'][vert_to][1]],
                 alpha=0.8, color=color_from_data, lw=4)
    plt.axis('equal')
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('init_file')
    args = parser.parse_args()
    path = args.init_file

    print("plotting: ", os.path.basename(args.init_file))

    counts, data = ip.parse_init(path)
    plot_init(counts, data)
