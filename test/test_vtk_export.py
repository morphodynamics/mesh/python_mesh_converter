import unittest
import os
import tempfile

import pyvista as pv

from converter_py.ply_parser import read_ply_mesh
from converter_py.vtk_export import write_vtk_mesh


class TestVtkExport(unittest.TestCase):

    def setUp(self):
        self.ply_filename = "example_data/hexagonal_prisms.ply"

        self.temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".vtk")

        self.mesh = read_ply_mesh(self.ply_filename)

    def tearDown(self):
        if os.path.exists(self.temp_file.name):
            os.remove(self.temp_file.name)

    def test_ply_writer(self):
        write_vtk_mesh(
            self.temp_file.name, self.mesh,
            save_edges=False
        )

        assert os.path.exists(self.temp_file.name)

        read_unstructured_grid = pv.read(self.temp_file.name)

        assert read_unstructured_grid.n_points == len(self.mesh.vertices)
        n_lines = read_unstructured_grid.extract_cells_by_type(cell_types=pv.CellType.LINE).n_cells
        assert n_lines == 0 # save_edges=False
        n_polygons = read_unstructured_grid.extract_cells_by_type(cell_types=pv.CellType.POLYGON).n_cells
        assert n_polygons == len(self.mesh.faces)
