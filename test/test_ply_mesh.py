import unittest
import os
import tempfile

from converter_py.ply_parser import read_ply_mesh, write_ply_mesh


class TestPlyMesh(unittest.TestCase):

    def setUp(self):
        self.ply_filename = "example_data/hexagonal_prisms.ply"

        self.temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".ply")

        self.mesh = read_ply_mesh(self.ply_filename)

    def tearDown(self):
        if os.path.exists(self.temp_file.name):
            os.remove(self.temp_file.name)

    def test_ply_writer(self):
        write_ply_mesh(self.temp_file.name, self.mesh)

        assert os.path.exists(self.temp_file.name)

        read_mesh = read_ply_mesh(self.temp_file.name)

        for element_name, elements in self.mesh.items():
            read_elements = read_mesh[element_name]
            assert len(read_elements) == len(elements)
